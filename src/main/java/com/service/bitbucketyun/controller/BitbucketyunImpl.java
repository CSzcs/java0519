package com.service.bitbucketyun.controller;


import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.apache.servicecomb.provider.rest.common.RestSchema;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.CseSpringDemoCodegen", date = "2018-05-19T16:56:42.096Z")

@RestSchema(schemaId = "bitbucketyun")
@RequestMapping(path = "/bitbucket-yun", produces = MediaType.APPLICATION_JSON)
public class BitbucketyunImpl {

    @Autowired
    private BitbucketyunDelegate userBitbucketyunDelegate;


    @RequestMapping(value = "/helloworld",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    public String helloworld( @RequestParam(value = "name", required = true) String name){

        return userBitbucketyunDelegate.helloworld(name);
    }

}
